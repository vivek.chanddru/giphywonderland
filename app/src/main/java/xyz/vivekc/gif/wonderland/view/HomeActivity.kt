package xyz.vivekc.gif.wonderland.view

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.View
import xyz.vivekc.gif.wonderland.BR
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.adapter.SectionsPagerAdapter
import xyz.vivekc.gif.wonderland.base.BaseLifeCycleActivity
import xyz.vivekc.gif.wonderland.databinding.ActivityHomeBinding
import xyz.vivekc.gif.wonderland.viewmodel.HomeActivityViewModel


class HomeActivity : BaseLifeCycleActivity<ActivityHomeBinding, HomeActivityViewModel>() {
    override val viewModelClass = HomeActivityViewModel::class.java

    override fun getBindingVariable() = BR.viewModel

    override fun getLayoutId() = R.layout.activity_home

    private val mSectionsPagerAdapter: SectionsPagerAdapter by lazy { SectionsPagerAdapter(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(viewDataBinding.toolbar)

        setupPager()

    }

    private fun setupPager() {
        viewDataBinding.container.adapter = mSectionsPagerAdapter
//        viewDataBinding.tabs.setupWithViewPager(viewDataBinding.container)
        viewDataBinding.container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(viewDataBinding.tabs))
        viewDataBinding.tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewDataBinding.container))


        mSectionsPagerAdapter.toggleView = {
            if (it) {
                viewDataBinding.tabs.visibility = View.VISIBLE
                mSectionsPagerAdapter.restrictSwipe = false
                mSectionsPagerAdapter.notifyDataSetChanged()
            } else {
                mSectionsPagerAdapter.restrictSwipe = true
                mSectionsPagerAdapter.notifyDataSetChanged()
                viewDataBinding.tabs.visibility = View.GONE
            }
        }
    }

}