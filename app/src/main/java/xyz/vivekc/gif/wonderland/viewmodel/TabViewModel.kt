package xyz.vivekc.gif.wonderland.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel

class TabViewModel(application: Application): AndroidViewModel(application)