package xyz.vivekc.gif.wonderland.data.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.reflect.TypeToken
import java.util.Collections.emptyList
import com.google.gson.Gson
import xyz.vivekc.gif.wonderland.data.model.Data
import java.util.*


class ImageTypeConvertor {


    companion object {
        internal var gson = Gson()

        @TypeConverter
        fun stringToSomeObjectList(data: String?): List<Data> {
            if (data == null) {
                return Collections.emptyList()
            }

            val listType = object : TypeToken<List<Data>>() {}.type

            return gson.fromJson(data, listType)
        }

        @TypeConverter
        fun someObjectListToString(someObjects: List<Data>): String {
            return gson.toJson(someObjects)
        }
    }
}