package xyz.vivekc.gif.wonderland.data.api

sealed class ResponseState<out T> {
    class ResponseLoading<out T>(tag: String = "") : ResponseState<T>() {
        override fun toString(): String {
            return "Response Loading :: tag"
        }
    }

    data class ResponseSuccess<out T>(val data: T) : ResponseState<T>()
    data class ResponseError<out T>(val throwable: Throwable) : ResponseState<T>()
}