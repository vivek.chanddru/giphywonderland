package xyz.vivekc.gif.wonderland.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Maybe
import xyz.vivekc.gif.wonderland.data.model.GifData


@Dao
interface FavoritesDao {

    @Query("SELECT * from GifData")
    fun getFavorites(): Flowable<List<GifData>>


    @Insert(onConflict = REPLACE)
    fun addFavorites(data: GifData): Long

    @Delete
    fun removeFavorites(data: GifData): Int

    @Query("SELECT * from GifData where gif_id = :gifId")
    fun isFavorite(gifId: String): Maybe<List<GifData>>
}