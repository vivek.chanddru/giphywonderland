package xyz.vivekc.gif.wonderland.base

import android.support.v7.widget.SearchView
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


fun SearchView.getObservable(): Flowable<String> {
    val subject = PublishSubject.create<String>()

    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(s: String): Boolean {
            subject.onComplete()
            return true
        }

        override fun onQueryTextChange(text: String): Boolean {
            subject.onNext(text)
            return true
        }
    })
    return subject.toFlowable(BackpressureStrategy.LATEST)
}