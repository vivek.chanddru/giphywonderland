package xyz.vivekc.gif.wonderland.data.model

import com.google.gson.annotations.SerializedName
import xyz.vivekc.gif.wonderland.base.BaseModel

data class Data(

    //we use only images for now. other params can be used if we show a details page :)
    @SerializedName("id")
    var id: String = "",

    @SerializedName("title")
    var title: String = "",

    @SerializedName("images")
    var images: Images = Images(),

    //todo use the below params as required when we do details page for each gif

    @SerializedName("_score")
    val score: Double = 0.0,

    @SerializedName("bitly_gif_url")
    val bitlyGifUrl: String = "",

    @SerializedName("bitly_url")
    val bitlyUrl: String = "",

    @SerializedName("content_url")
    val contentUrl: String = "",

    @SerializedName("embed_url")
    val embedUrl: String = "",

    @SerializedName("import_datetime")
    val importDatetime: String = "",

    @SerializedName("is_sticker")
    val isSticker: Int = 0,

    @SerializedName("rating")
    val rating: String = "",

    @SerializedName("slug")
    val slug: String = "",

    @SerializedName("source")
    val source: String = "",

    @SerializedName("source_post_url")
    val sourcePostUrl: String = "",

    @SerializedName("source_tld")
    val sourceTld: String = "",

    @SerializedName("trending_datetime")
    val trendingDatetime: String = "",

    @SerializedName("type")
    val type: String = "",

    @SerializedName("url")
    val url: String = "",

    @SerializedName("user")
    val user: User = User(),

    @SerializedName("username")
    val username: String = ""
) : BaseModel