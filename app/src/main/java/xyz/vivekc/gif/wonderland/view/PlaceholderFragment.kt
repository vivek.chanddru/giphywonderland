package xyz.vivekc.gif.wonderland.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.view.*
import xyz.vivekc.gif.wonderland.BR
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.base.BaseLifeCycleFragment
import xyz.vivekc.gif.wonderland.databinding.FragmentHomeBinding
import xyz.vivekc.gif.wonderland.viewmodel.TabViewModel

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : BaseLifeCycleFragment<FragmentHomeBinding,TabViewModel>() {
    override val viewModelClass = TabViewModel::class.java

    override fun getBindingVariable()= BR.tabViewModel

    override fun getLayoutId() = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding.sectionLabel.text = getString(R.string.section_format, arguments?.getInt(ARG_SECTION_NUMBER))

    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            val fragment = PlaceholderFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}