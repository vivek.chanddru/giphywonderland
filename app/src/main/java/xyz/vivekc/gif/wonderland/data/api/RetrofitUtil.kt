package xyz.vivekc.gif.wonderland.data.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitUtil {
    private val BASE_URL = "https://api.giphy.com/v1/gifs/"
    private var retrofitInstance: Retrofit? = null
    private var giphyApi: GiphyApi? = null
    private val httpLoggingInterceptor by lazy { HttpLoggingInterceptor() }
    private val apiKeyQueryParameterInterceptor by lazy {
        Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", "GOXG0XoAmbujeND3C7H5WEacG3Q5yMjS")
                .build()

            val requestBuilder = original.newBuilder()
                .url(url)

            val request = requestBuilder.build()
            return@Interceptor chain.proceed(request)
        }
    }


    fun getRetrofitInstance(): Retrofit {
        if (retrofitInstance == null) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            retrofitInstance = Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    OkHttpClient.Builder()
                        .addInterceptor(apiKeyQueryParameterInterceptor)
                        .addInterceptor(httpLoggingInterceptor)
                        .build()
                ).build()
        }
        return retrofitInstance!! //throw NPE if there is an issue. No use in chaining it if retrofit is null
    }


    fun getGiphyApiService(): GiphyApi {
        if (giphyApi == null) {
            giphyApi = getRetrofitInstance().create(GiphyApi::class.java)
        }
        return giphyApi!!//throw NPE if there is an issue. No use in chaining it if API Service is null
    }
}