package xyz.vivekc.gif.wonderland.data.model

import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("downsized")
    val downsized: GifInfo = GifInfo(),
    @SerializedName("fixed_height")
    val fixedHeight: GifInfo = GifInfo(),
    @SerializedName("fixed_width")
    val fixedWidth: GifInfo = GifInfo(),
    @SerializedName("original")
    val original: GifInfo = GifInfo(),
    @SerializedName("preview_gif")
    val previewGif: GifInfo = GifInfo(),
    @SerializedName("480w_still")
    val staticPreview: GifInfo = GifInfo()
)