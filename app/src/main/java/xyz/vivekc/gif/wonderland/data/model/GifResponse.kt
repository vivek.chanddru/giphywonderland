package xyz.vivekc.gif.wonderland.data.model

import com.google.gson.annotations.SerializedName

data class GifResponse(
    @SerializedName("data")
    val data: List<Data> = listOf(),
    @SerializedName("meta")
    val meta: Meta = Meta(),
    @SerializedName("pagination")
    val pagination: Pagination = Pagination()
)