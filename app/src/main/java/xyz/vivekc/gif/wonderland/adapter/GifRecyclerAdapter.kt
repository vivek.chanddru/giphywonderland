package xyz.vivekc.gif.wonderland.adapter

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.support.v7.util.DiffUtil
import xyz.vivekc.gif.wonderland.BR
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.base.BaseRecyclerAdapter
import xyz.vivekc.gif.wonderland.data.model.GifData
import xyz.vivekc.gif.wonderland.databinding.GifItemLayoutBinding
import xyz.vivekc.gif.wonderland.util.BiCallback
import xyz.vivekc.gif.wonderland.viewmodel.GifItemViewModel


class GifRecyclerAdapter(val application: Application) :
    BaseRecyclerAdapter<GifItemViewHolder, GifItemLayoutBinding, GifData>() {

    var favClickedListener: BiCallback<GifData, Int>? = null
    override val layoutResId: Int = R.layout.gif_item_layout
    override fun getHolderInstance(binding: GifItemLayoutBinding): GifItemViewHolder {
        return GifItemViewHolder(application, binding, favClickedListener)
    }

    override fun onBindViewHolder(holder: GifItemViewHolder, position: Int) {
        val binding = (holder.itemBinding as GifItemLayoutBinding)
        binding.renderer.layout(0, 0, 0, 0)
        super.onBindViewHolder(holder, position)
    }

    fun updateList(newList: ArrayList<GifData>, replaceAll: Boolean = false) {
        if (replaceAll) {
            this.listItems = newList
            notifyDataSetChanged()
        } else {
            if (this.listItems.size == 0) {
                this.listItems = newList
                notifyDataSetChanged()
            }
            if (newList.size == 0) {
                this.listItems = newList
                notifyDataSetChanged()
            } else {
                val diffResult = DiffUtil.calculateDiff(DiffCallback(this.listItems, newList))
                diffResult.dispatchUpdatesTo(this)
                notifyDataSetChanged()
            }
        }
    }
}


class GifItemViewHolder(
    val application: Application,
    val binding: GifItemLayoutBinding,
    val favClickedListener: BiCallback<GifData, Int>?
) : BaseRecyclerAdapter.BaseViewHolder<GifData>(binding) {
    override fun getBindingVariable(): Int = BR.itemViewModel

    override fun getViewModelInstance(item: GifData): AndroidViewModel {
        binding.renderer.layout(0, 0, 0, 0)
        val viewModel = GifItemViewModel(item, application)
        viewModel.favClickListener = {
            favClickedListener?.invoke(it, adapterPosition)
        }
        return viewModel
    }
}


class DiffCallback(private var newList: List<GifData>, private var oldList: List<GifData>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id === newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}