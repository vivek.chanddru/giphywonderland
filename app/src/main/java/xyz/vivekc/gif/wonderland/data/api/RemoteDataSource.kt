package xyz.vivekc.gif.wonderland.data.api

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.data.GiphyDataSource
import xyz.vivekc.gif.wonderland.data.model.GifData

class RemoteDataSource : GiphyDataSource {

    override fun getTrendingGifs(limit: Int, offset: Int): Flowable<List<GifData>> {
        return RetrofitUtil.getGiphyApiService().getTrendingGifs(limit, offset).map { it ->
            val list = mutableListOf<GifData>()
            it.data.forEach {
                list.add(GifData(it.id, it.title, it.images.fixedWidth.url, false, it.images.fixedWidth.width.toFloat(), it.images.fixedWidth.height.toFloat()))
            }

            return@map list
        }
    }

    override fun getSearchResults(searchStr: String): Flowable<List<GifData>> {
        return RetrofitUtil.getGiphyApiService().getSearchResult(searchStr).map { it ->
            val list = mutableListOf<GifData>()
            it.data.forEach {
                list.add(GifData(it.id, it.title, it.images.fixedWidth.url, false, it.images.fixedWidth.width.toFloat(), it.images.fixedWidth.height.toFloat()))
            }

            return@map list
        }
    }


    //We do not have to use the remote data source to set or get favorites. So, we don't use them
    @Throws(RuntimeException::class)
    override fun getFavorites(): Flowable<List<GifData>> {
        throw RuntimeException("Do not call methods related to favorites on Remote Data Source")
    }

    @Throws(RuntimeException::class)
    override fun saveAsFavorite(data: GifData): Single<Long> {
        throw RuntimeException("Do not call methods related to favorites on Remote Data Source")
    }

    @Throws(RuntimeException::class)
    override fun isFavourite(id: String): Maybe<List<GifData>> {
        throw RuntimeException("Do not call methods related to favorites on Remote Data Source")
    }

    @Throws(RuntimeException::class)
    override fun removeAsFavorite(data: GifData): Single<Int> {
        throw RuntimeException("Do not call methods related to favorites on Remote Data Source")
    }

}