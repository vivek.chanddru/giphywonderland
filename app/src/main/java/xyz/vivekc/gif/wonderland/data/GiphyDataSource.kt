package xyz.vivekc.gif.wonderland.data

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.data.model.Data
import xyz.vivekc.gif.wonderland.data.model.GifData
import xyz.vivekc.gif.wonderland.data.model.GifResponse

interface GiphyDataSource {
    fun getTrendingGifs(limit: Int = 25, offset: Int = 0): Flowable<List<GifData>>
    fun getSearchResults(searchStr: String): Flowable<List<GifData>>
    fun getFavorites(): Flowable<List<GifData>>
    fun saveAsFavorite(data: GifData): Single<Long>
    fun isFavourite(id: String): Maybe<List<GifData>>
    fun removeAsFavorite(data: GifData): Single<Int>
}