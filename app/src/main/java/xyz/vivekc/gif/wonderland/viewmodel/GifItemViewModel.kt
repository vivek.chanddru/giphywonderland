package xyz.vivekc.gif.wonderland.viewmodel

import android.app.Application
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.base.BaseViewModel
import xyz.vivekc.gif.wonderland.data.model.GifData
import xyz.vivekc.gif.wonderland.util.Callback


class GifItemViewModel(val data: GifData, application: Application) : BaseViewModel(application) {

    var gifPreviewUrl = ObservableField<String>()

    var favClickListener: Callback<GifData>? = null

    init {
        gifPreviewUrl.set(data.imageUrl)
    }


    fun getFavoriteIcon(): Int {
        return if (data.isFavorite) {
            R.drawable.favorite_red
        } else {
            R.drawable.fav_outline
        }
    }

    fun onFavClicked(view: View) {
        favClickListener?.invoke(data)
    }


    companion object {
        @JvmStatic
        @BindingAdapter("imageData")
        fun loadImage(view: ImageView, gifData: GifData) {

            val viewWidth = view.measuredWidth
            val widthRatio = viewWidth / gifData.width

            val viewHeight = gifData.height * widthRatio
            Log.d(
                "LoadingImage",
                gifData.imageUrl + " " + view.measuredWidth + " " + view.width + " " + gifData.width + " " + gifData.height
            )
            Glide.with(view.context)
                .asGif()
                .load(gifData.imageUrl)
                .apply(RequestOptions().override(view.width, viewHeight.toInt()))
                .into(view)
        }


        @JvmStatic
        @BindingAdapter("android:src")
        fun setImageResource(imageView: ImageView, resource: Int) {
            if (resource != 0) {
                imageView.setImageResource(resource)
            }
        }

    }
}