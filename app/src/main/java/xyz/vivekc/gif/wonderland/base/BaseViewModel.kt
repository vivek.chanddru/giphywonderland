package xyz.vivekc.gif.wonderland.base

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable
import xyz.vivekc.gif.wonderland.data.api.Providers

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val compositeDisposable by lazy { CompositeDisposable() }
    val giphyRepository by lazy {
        Providers.provideGiphyRepository(
            Providers.provideRemoteGiphyRepository(),
            Providers.provideLocalGiphyRepository(application)
        )
    }

    val schedulers by lazy {
        Providers.provideSchedulers()
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}