package xyz.vivekc.gif.wonderland.view

import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.vivekc.gif.wonderland.BR
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.adapter.GifRecyclerAdapter
import xyz.vivekc.gif.wonderland.base.BaseLifeCycleFragment
import xyz.vivekc.gif.wonderland.data.api.ResponseState
import xyz.vivekc.gif.wonderland.databinding.FragmentFavoriteBinding
import xyz.vivekc.gif.wonderland.viewmodel.FavoriteTabViewModel
import xyz.vivekc.gif.wonderland.viewmodel.HomeActivityViewModel

class FavoriteFragment : BaseLifeCycleFragment<FragmentFavoriteBinding, FavoriteTabViewModel>() {
    override val viewModelClass = FavoriteTabViewModel::class.java

    override fun getBindingVariable() = BR.favViewModel

    override fun getLayoutId() = R.layout.fragment_favorite

    val adapter by lazy { GifRecyclerAdapter(requireActivity().application) }

    val activityViewModel by lazy {
        activity?.let {
            ViewModelProviders.of(it).get(HomeActivityViewModel::class.java)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewDataBinding.gifView.adapter = adapter
        viewDataBinding.gifView.layoutManager = GridLayoutManager(requireContext(), 2)

        handleFavClick()

        compositeDisposable.add(viewModel.getFavorites().subscribe {
            when (it) {
                is ResponseState.ResponseSuccess -> {
                    if (it.data.isEmpty()) {
                        showEmptyView()
                    } else {
                        hideEmptyView()
                        viewModel.checkIfFavorite(it.data)
                        adapter.listItems = ArrayList(it.data)
                    }
                }
            }
        })
    }

    private fun showEmptyView() {
        viewDataBinding.gifView.visibility = View.GONE
        viewDataBinding.noResultView.visibility = View.VISIBLE
    }

    private fun hideEmptyView() {
        viewDataBinding.gifView.visibility = View. VISIBLE
        viewDataBinding.noResultView.visibility = View.GONE
    }

    private fun handleFavClick() {
        adapter.favClickedListener = { data, position ->

            if (data.isFavorite) {
                //already fav
                compositeDisposable.add(viewModel.removeFavoriteItem(data).subscribe { deletionCount, error ->
                    if (error == null) {
                        println("Removed $deletionCount $data")
                        viewModel.updateFavoriteModel(data, false)
                        adapter.notifyItemChanged(position)

                        activityViewModel?.unFavoriteLiveData?.postValue(data)
                    } else {
                        error.printStackTrace()
                    }
                })
            }
        }
    }
}