package xyz.vivekc.gif.wonderland.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import io.reactivex.Observable
import xyz.vivekc.gif.wonderland.data.model.GifData

class HomeActivityViewModel(application: Application): AndroidViewModel(application) {

    val unFavoriteLiveData = MutableLiveData<GifData>()
}