package xyz.vivekc.gif.wonderland.base

import android.arch.lifecycle.AndroidViewModel
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

interface BaseModel

abstract class BaseRecyclerAdapter<T : BaseRecyclerAdapter.BaseViewHolder<M>, B : ViewDataBinding, M : BaseModel> :
    RecyclerView.Adapter<T>() {

    var listItems: ArrayList<M> = ArrayList()
        set(value) {
            field.clear()
            field.addAll(value)
            notifyDataSetChanged()
        }

    abstract val layoutResId: Int

    abstract fun getHolderInstance(binding: B): T


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): T {
        val itemBinding = DataBindingUtil.inflate<B>(LayoutInflater.from(parent.context), layoutResId, parent, false)
        return getHolderInstance(itemBinding)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun getItemId(position: Int): Long {
        return listItems[position].hashCode().toLong()
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        holder.bind(listItems[position])
    }


    abstract class BaseViewHolder<M : BaseModel>(val itemBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        abstract fun getBindingVariable(): Int
        abstract fun getViewModelInstance(item: M): AndroidViewModel

        fun bind(item: M) {
            val result = itemBinding.setVariable(getBindingVariable(), getViewModelInstance(item))
            if (!result) {
                throw RuntimeException("ViewModel variable not set. Check the types")
            }
        }
    }
}
