package xyz.vivekc.gif.wonderland.base

import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable

abstract class BaseLifeCycleFragment<B : ViewDataBinding, T : AndroidViewModel> : Fragment() {

    val compositeDisposable by lazy { CompositeDisposable() }

    abstract val viewModelClass: Class<T>

    protected open val viewModel: T by lazy {
        ViewModelProviders.of(this).get(viewModelClass)
    }


    lateinit var viewDataBinding: B

    var rootView: View? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        rootView = viewDataBinding.root
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val result = viewDataBinding.setVariable(getBindingVariable(), viewModel)
        if (!result) {
            throw RuntimeException("ViewModel variable not set. Check the types")
        }
        viewDataBinding.executePendingBindings()
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
    //  data binding variable
    abstract fun getBindingVariable(): Int

    //  layout id
    @LayoutRes
    abstract fun getLayoutId(): Int

}