package xyz.vivekc.gif.wonderland.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import xyz.vivekc.gif.wonderland.base.BaseModel


@Entity(tableName = "GifData")
data class GifData(
    @ColumnInfo(name = "gif_id")
    @PrimaryKey()
    var id: String = "",

    @ColumnInfo(name = "gif_title")
    var title: String = "",

    @ColumnInfo(name = "gif_image")
    var imageUrl: String = "",

    @ColumnInfo(name = "is_fav")
    @Ignore
    var isFavorite: Boolean = false,

    @ColumnInfo(name = "width")
    var width: Float = 0.0f,
    @ColumnInfo(name = "height")
    var height: Float = 0.0f
) : BaseModel