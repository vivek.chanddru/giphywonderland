package xyz.vivekc.gif.wonderland.data.api

import android.content.Context
import xyz.vivekc.gif.wonderland.data.GiphyDataSource
import xyz.vivekc.gif.wonderland.data.GiphyRepository
import xyz.vivekc.gif.wonderland.data.api.schedulers.BaseSchedulerProvider
import xyz.vivekc.gif.wonderland.data.api.schedulers.SchedulerProvider
import xyz.vivekc.gif.wonderland.data.db.LocalDataSource

object Providers {

    private val schedulerProvider by lazy { SchedulerProvider() }
    fun provideGiphyRepository(remoteDataSource: GiphyDataSource, localDataSource: GiphyDataSource): GiphyRepository {
        return GiphyRepository(remoteDataSource, localDataSource)
    }

    fun provideRemoteGiphyRepository(): GiphyDataSource {
        return RemoteDataSource()
    }

    fun provideLocalGiphyRepository(context: Context): GiphyDataSource {
        return LocalDataSource(context)
    }

    fun provideSchedulers(): BaseSchedulerProvider {
        return schedulerProvider
    }
}
