package xyz.vivekc.gif.wonderland.data.db

import android.content.Context
import android.database.sqlite.SQLiteException
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.data.GiphyDataSource
import xyz.vivekc.gif.wonderland.data.model.GifData
import java.util.*

class LocalDataSource(val context: Context) : GiphyDataSource {

    override fun getFavorites(): Flowable<List<GifData>> {
        return FavoritesDatabase.getInstance(context)?.favoritesDao()?.getFavorites() ?: Flowable.fromIterable(
            Collections.emptyList()
        )
    }

    override fun saveAsFavorite(data: GifData): Single<Long> {

        return Single.create<Long> {
            val a = FavoritesDatabase.getInstance(context)?.favoritesDao()?.addFavorites(data)
            println(a)
            if (a != null && a != 0L) {
                it.onSuccess(a)
            } else {
                it.onError(SQLiteException("Error in inserting"))
            }
        }
    }

    override fun removeAsFavorite(data: GifData): Single<Int> {
        return Single.create<Int> {
            val a = FavoritesDatabase.getInstance(context)?.favoritesDao()?.removeFavorites(data)
            if (a != null && a != 0) {
                it.onSuccess(a)
            } else {
                it.onError(SQLiteException("Error in deletion"))
            }
        }
    }

    override fun isFavourite(id: String): Maybe<List<GifData>> {
        return FavoritesDatabase.getInstance(context)?.favoritesDao()?.isFavorite(id)
            ?: Maybe.just(Collections.emptyList())
    }

    //No need for DB Fetch for trending and search
    override fun getTrendingGifs(limit: Int, offset: Int): Flowable<List<GifData>> {
        throw RuntimeException("Do not call methods related to Trending on Local Data Source")
    }

    override fun getSearchResults(searchStr: String): Flowable<List<GifData>> {
        throw RuntimeException("Do not call methods related to Trending on Local Data Source")
    }


}