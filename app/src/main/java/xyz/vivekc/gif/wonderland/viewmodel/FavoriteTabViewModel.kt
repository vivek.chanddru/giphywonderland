package xyz.vivekc.gif.wonderland.viewmodel

import android.app.Application
import io.reactivex.Flowable
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.base.BaseViewModel
import xyz.vivekc.gif.wonderland.data.api.ResponseState
import xyz.vivekc.gif.wonderland.data.model.GifData

class FavoriteTabViewModel(application: Application) : BaseViewModel(application) {
    fun getFavorites(): Flowable<ResponseState<List<GifData>>> {
        return giphyRepository.getFavorites()
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
            .map { gifList ->
                gifList.forEach {
                    it.isFavorite = true
                }
                return@map gifList
            }
            .map<ResponseState<List<GifData>>> { ResponseState.ResponseSuccess(it) }
            .startWith(ResponseState.ResponseLoading())
            .onErrorReturn { ResponseState.ResponseError(it) }

    }

    fun favoriteItem(data: GifData): Single<Long> {
        return giphyRepository.saveAsFavorite(data)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
    }

    fun removeFavoriteItem(data: GifData): Single<Int> {
        return giphyRepository.removeAsFavorite(data)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
    }


    fun checkIfFavorite(gifDataList: List<GifData>) {
        gifDataList.forEach { data ->
            giphyRepository.isFavourite(data.id)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe { items ->
                    data.isFavorite = !items.isEmpty()
                }
        }
    }

    fun updateFavoriteModel(data: GifData, isFav: Boolean) {
        data.isFavorite = isFav
    }
}