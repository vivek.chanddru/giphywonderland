package xyz.vivekc.gif.wonderland.util

import android.content.Context
import android.graphics.Point

typealias Callback<T> = (T) -> Unit
typealias BiCallback<T,U> = (T,U) -> Unit

fun Context.getDeviceResolution(): Point {
    val displayMetrics = resources.displayMetrics
    val displayWidth = displayMetrics.widthPixels
    val displayHeight = displayMetrics.heightPixels
    return Point(displayWidth, displayHeight)
}