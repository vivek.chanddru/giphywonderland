package xyz.vivekc.gif.wonderland.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import xyz.vivekc.gif.wonderland.util.Callback
import xyz.vivekc.gif.wonderland.view.FavoriteFragment
import xyz.vivekc.gif.wonderland.view.TrendingFragment
import xyz.vivekc.gif.wonderland.view.TrendingToActivityInterface

class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var toggleView: Callback<Boolean>? = null
    var restrictSwipe = false
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                val trendingFragment = TrendingFragment()
                trendingFragment.fragmentInterface = object : TrendingToActivityInterface {
                    override fun toggleTabs(shouldShow: Boolean) {
                        toggleView?.invoke(shouldShow)
                    }
                }
                trendingFragment
            }
            1 -> FavoriteFragment()
            else -> Fragment() //this shouldn't happen at all
        }
    }

    override fun getCount(): Int {
        return if (restrictSwipe) 1 else 2
    }
}