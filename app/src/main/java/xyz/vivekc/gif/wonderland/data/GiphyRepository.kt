package xyz.vivekc.gif.wonderland.data

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.data.model.GifData

class GiphyRepository(private var remoteDataSource: GiphyDataSource, private var localDataSource: GiphyDataSource) :
    GiphyDataSource {


    override fun getTrendingGifs(limit: Int, offset: Int): Flowable<List<GifData>> {
        return remoteDataSource.getTrendingGifs(limit,offset)
    }

    override fun getSearchResults(searchStr: String): Flowable<List<GifData>> {
        return remoteDataSource.getSearchResults(searchStr)
    }

    override fun getFavorites(): Flowable<List<GifData>> {
        return localDataSource.getFavorites()
    }

    override fun isFavourite(id: String): Maybe<List<GifData>> {
        return localDataSource.isFavourite(id)
    }

    override fun saveAsFavorite(data: GifData): Single<Long> {
        return localDataSource.saveAsFavorite(data)
    }

    override fun removeAsFavorite(data: GifData): Single<Int> {
        return localDataSource.removeAsFavorite(data)
    }
}