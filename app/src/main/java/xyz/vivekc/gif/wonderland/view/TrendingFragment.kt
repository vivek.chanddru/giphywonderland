package xyz.vivekc.gif.wonderland.view

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import xyz.vivekc.gif.wonderland.BR
import xyz.vivekc.gif.wonderland.R
import xyz.vivekc.gif.wonderland.adapter.GifRecyclerAdapter
import xyz.vivekc.gif.wonderland.base.BaseLifeCycleFragment
import xyz.vivekc.gif.wonderland.base.PermissionResult
import xyz.vivekc.gif.wonderland.base.askForPermission
import xyz.vivekc.gif.wonderland.base.getObservable
import xyz.vivekc.gif.wonderland.data.api.ResponseState
import xyz.vivekc.gif.wonderland.data.model.GifData
import xyz.vivekc.gif.wonderland.databinding.FragmentTrendingBinding
import xyz.vivekc.gif.wonderland.viewmodel.HomeActivityViewModel
import xyz.vivekc.gif.wonderland.viewmodel.TrendingTabViewModel
import java.util.concurrent.TimeUnit

class TrendingFragment : BaseLifeCycleFragment<FragmentTrendingBinding, TrendingTabViewModel>() {


    //TrendingFragment related members
    override val viewModelClass = TrendingTabViewModel::class.java

    override fun getBindingVariable() = BR.trendingViewModel

    override fun getLayoutId() = R.layout.fragment_trending


    //activity Scoped viewmodel for communication between fragments inside same activity
    val activityViewModel by lazy {
        activity?.let {
            ViewModelProviders.of(it).get(HomeActivityViewModel::class.java)
        }
    }

    //adapter
    val adapter by lazy { GifRecyclerAdapter(requireActivity().application) }


    //SearchView stuff
    private var isSearching: Boolean = false
    var searchView: SearchView? = null

    var fragmentInterface: TrendingToActivityInterface? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        handleFavClick()
        loadTrendingGifs(0, 25)

        subscribeToUnfav()

        viewDataBinding.swipeRefreshLayout.setOnRefreshListener {
            if (!isSearching) {
                loadTrendingGifs(0, 25)
            } else {
                searchView?.let { handleSearch(it) }
            }
        }
    }


    private fun setupAdapter() {
        viewDataBinding.gifView.adapter = adapter
        val layoutManager = LinearLayoutManager(requireContext())
        viewDataBinding.gifView.layoutManager = layoutManager
    }

    private fun handleFavClick() {
        adapter.favClickedListener = { data, position ->

            if (data.isFavorite) {
                //already fav
                compositeDisposable.add(viewModel.removeFavoriteItem(data).subscribe { deletionCount, error ->
                    if (error == null) {
                        println("Removed $deletionCount $data")
                        viewModel.updateFavoriteModel(data, false)
                        adapter.notifyItemChanged(position)
                    } else {
                        error.printStackTrace()
                    }
                })
            } else {
                requireContext().askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) { permissionResult ->
                    when (permissionResult) {
                        PermissionResult.GRANTED -> {
                            //mark as fav
                            compositeDisposable.add(viewModel.favoriteItem(data)
                                .subscribe { file, error ->
                                    if (error == null) {
                                        println("Added $file $data")
                                        viewModel.updateFavoriteModel(data, true)

                                        adapter.notifyItemChanged(position)
                                    } else {
                                        error.printStackTrace()
                                    }
                                })
                        }
                        PermissionResult.DENIED -> {
                            //User clicked just deny. show toast or snackbar
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.storage_permission_required_text),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

            }
        }
    }

    private fun loadTrendingGifs(offset: Int, limit: Int) {
        compositeDisposable.add(
            viewModel.loadTrendingGifs(offset, limit)
                .subscribe({ responseState ->
                    handleResponse(responseState)
                }, {
                    it.printStackTrace()
                })
        )
    }

    private fun subscribeToUnfav() {
        activityViewModel?.unFavoriteLiveData?.observe(this, Observer {
            it?.let { gifData ->
                viewModel.checkIfFavorite(adapter.listItems)
                adapter.notifyDataSetChanged()
            }
        })
    }


    private fun handleResponse(responseState: ResponseState<List<GifData>>) {
        when (responseState) {
            is ResponseState.ResponseLoading -> {
                viewDataBinding.swipeRefreshLayout.isRefreshing = true
            }

            is ResponseState.ResponseSuccess -> {
                viewDataBinding.swipeRefreshLayout.isRefreshing = false
                if (responseState.data.isEmpty()) {
                    showEmptyView()
                } else {
                    hideEmptyView()
                    println(responseState.data)
                    viewModel.checkIfFavorite(responseState.data)
                    adapter.updateList(ArrayList(responseState.data), true)
                }
            }

            is ResponseState.ResponseError -> {
                viewDataBinding.swipeRefreshLayout.isRefreshing = false
                println("Error happened")
                responseState.throwable.printStackTrace()
            }
        }
    }


    private fun showEmptyView() {
        viewDataBinding.gifView.visibility = View.GONE
        viewDataBinding.noResultView.visibility = View.VISIBLE
    }

    private fun hideEmptyView() {
        viewDataBinding.gifView.visibility = View.VISIBLE
        viewDataBinding.noResultView.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_home, menu)

        val myActionMenuItem = menu?.findItem(R.id.action_search)
        searchView = myActionMenuItem?.actionView as SearchView

        searchView?.setOnQueryTextFocusChangeListener { v, hasFocus ->
            println("hasFocus $hasFocus")
            isSearching = hasFocus
            fragmentInterface?.toggleTabs(!hasFocus)
        }

        searchView?.let {
            handleSearch(it)
        }
    }

    private fun handleSearch(searchView: SearchView) {
        compositeDisposable.add(
            searchView.getObservable()
                .distinctUntilChanged()
                .debounce(300, TimeUnit.MILLISECONDS)
                .flatMap {
                    if (it.isEmpty()) {
                        viewModel.loadTrendingGifs(0, 25)
                    } else {
                        viewModel.subscribeToSearchResult(it)
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ responseState ->
                    handleResponse(responseState)
                }, {
                    it.printStackTrace()
                })
        )
    }
}

interface TrendingToActivityInterface {
    fun toggleTabs(shouldShow: Boolean)
}