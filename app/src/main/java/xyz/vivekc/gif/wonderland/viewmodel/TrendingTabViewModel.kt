package xyz.vivekc.gif.wonderland.viewmodel

import android.app.Application
import android.content.Context
import android.os.Environment
import com.bumptech.glide.Glide
import io.reactivex.Flowable
import io.reactivex.Single
import xyz.vivekc.gif.wonderland.base.BaseViewModel
import xyz.vivekc.gif.wonderland.data.api.ResponseState
import xyz.vivekc.gif.wonderland.data.model.GifData
import java.io.File
import java.io.FileInputStream

class TrendingTabViewModel(application: Application) : BaseViewModel(application) {

    fun loadTrendingGifs(offset: Int, limit: Int): Flowable<ResponseState<List<GifData>>> {
        return giphyRepository.getTrendingGifs(limit, offset)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
            .map<ResponseState<List<GifData>>> { ResponseState.ResponseSuccess(it) }
            .startWith(ResponseState.ResponseLoading())
            .onErrorReturn { ResponseState.ResponseError(it) }
    }

    fun subscribeToSearchResult(searchFlowable: String): Flowable<ResponseState<List<GifData>>> {
        return giphyRepository.getSearchResults(searchFlowable)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
            .map<ResponseState<List<GifData>>> { ResponseState.ResponseSuccess(it) }
            .startWith(ResponseState.ResponseLoading())
            .onErrorReturn { ResponseState.ResponseError(it) }

    }

    fun favoriteItem(data: GifData): Single<Long> {
        return Single.create<File> {
            val fileSave = saveToFile(data)
            if (fileSave == null) {
                it.onError(NullPointerException())
            } else {
                data.imageUrl = fileSave.absolutePath
                it.onSuccess(fileSave)
            }
        }.flatMap {
            giphyRepository.saveAsFavorite(data)
        }
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
    }

    fun saveToFile(data: GifData): File? {
        val cacheFile = Glide.with(getApplication() as Context).downloadOnly().load(data.imageUrl).submit().get()

        val fis = FileInputStream(cacheFile)
        val dirPath = "${Environment.getExternalStorageDirectory()}/GiphyWonders/"
        val dir = File(dirPath)
        dir.mkdirs()
        if (dir.exists()) {
            var file = File(dirPath + data.title + ".gif")
            file = cacheFile.copyTo(file, true)
            println("FIle: ${file.name} ${file.length()} ${file.absolutePath}")
            return file
        }
        return null
    }

    fun removeFavoriteItem(data: GifData): Single<Int> {
        return giphyRepository.removeAsFavorite(data)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())
    }


    fun checkIfFavorite(gifDataList: List<GifData>) {
        gifDataList.forEach { data ->
            giphyRepository.isFavourite(data.id)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe { items ->
                    data.isFavorite = !items.isEmpty()
                }
        }
    }

    fun updateFavoriteModel( data: GifData, isFav: Boolean) {
        data.isFavorite = isFav
    }


}