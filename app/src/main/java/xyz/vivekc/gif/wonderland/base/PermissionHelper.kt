package xyz.vivekc.gif.wonderland.base

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


enum class PermissionResult {
    GRANTED, DENIED
}

fun Context.askForPermission(
    permissions: String,
    callbackFunction: (PermissionResult) -> Unit
) {

    val resultReceiver = PermissionResultReceiver(object : PermissionCallback {
        override fun onGranted() {
            callbackFunction(PermissionResult.GRANTED)
            removePermissionFragment(this@askForPermission)
        }

        override fun onDenied() {
            callbackFunction(PermissionResult.DENIED)
            removePermissionFragment(this@askForPermission)
        }

    })
    val fragment = PermissionHelper().apply {
        arguments = Bundle().apply {
            putString(PERMISSION_NAMES, permissions)
            putBoolean(PERMISSION_ASK_AGAIN, true) //if required, later make this a param sent by the app
            putParcelable(PERMISSION_RESULT_RECEIVER, resultReceiver)
        }
    }
    (this.scanForActivity() as? AppCompatActivity)
        ?.addFragment(fragment to "permission_helper")
}

fun openPermissionSettings(context: Context) {
    val intent = Intent().apply {
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        data = Uri.fromParts("package", context.packageName, null)
    }
    context.startActivity(intent)
}

class PermissionHelper : Fragment() {

    private val resultReceiver by lazy {
        arguments!!.getParcelable<PermissionResultReceiver>(PERMISSION_RESULT_RECEIVER)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        checkPermission(
            requireActivity().application, arguments?.getString(PERMISSION_NAMES, "")
                ?: ""
        )
    }

    @SuppressLint("NewApi")
    private fun checkPermission(application: Application, permissionNames: String) {
        val permissions = permissionNames.split(",").map { it.trim() }
        val granted = verifyPermissions(permissions)
        if (granted) {
            resultReceiver.send(PERMISSION_RES_CODE, Bundle()
                .apply {
                    putString(
                        PermissionResultReceiver.KEY,
                        PermissionResultReceiver.GRANTED
                    )
                }
            )
        } else {
            val askAgain = arguments?.getBoolean(PERMISSION_ASK_AGAIN) ?: true
            permissions.forEach { permission ->
                if (shouldShowRequestPermissionRationale(permission)) {
                    //previously denied. show rationale and ask permission
                    if (askAgain) {
                        permission.setAskedForPermissionEarlier(application, true)
                        askPermission(permissions)
                    } else {
                        resultReceiver.send(PERMISSION_RES_CODE, Bundle()
                            .apply {
                                putString(
                                    PermissionResultReceiver.KEY,
                                    PermissionResultReceiver.DENIED
                                )
                            }
                        )
                    }
                } else {
                    if (!permission.didAskedForPermissionEarlier(application)) {
                        //asking permission for first time.
                        permission.setAskedForPermissionEarlier(application, true)
                        askPermission(permissions)
                    } else {
                        //permission - never show again clicked
                        resultReceiver.send(PERMISSION_RES_CODE, Bundle()
                            .apply {
                                putString(
                                    PermissionResultReceiver.KEY,
                                    PermissionResultReceiver.DENIED
                                )
                            }
                        )
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQ_CODE) {
            if (verifyPermissions(grantResults)) {
                //all permissions granted
                resultReceiver.send(PERMISSION_RES_CODE, Bundle()
                    .apply {
                        putString(
                            PermissionResultReceiver.KEY,
                            PermissionResultReceiver.GRANTED
                        )
                    }
                )
            } else {
                //some or all permissions not granted
                resultReceiver.send(PERMISSION_RES_CODE, Bundle()
                    .apply {
                        putString(
                            PermissionResultReceiver.KEY,
                            PermissionResultReceiver.DENIED
                        )
                    }
                )
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun askPermission(permissionNames: List<String>) {
        val permissions = permissionNames
            .filterNot {
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    it
                ) == PackageManager.PERMISSION_GRANTED
            }
            .toTypedArray()
        requestPermissions(permissions, PERMISSION_REQ_CODE)
    }

    private fun verifyPermissions(permissions: List<String>): Boolean {
        if (!shouldAskPermission) {
            return true
        }
        return permissions.all {
            ActivityCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }
    }

    // Verify that each required permission has been granted, otherwise return false.
    private fun verifyPermissions(grantResults: IntArray) =
        grantResults.all { it == PackageManager.PERMISSION_GRANTED }

    private val shouldAskPermission = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    companion object {
        private const val PERMISSION_REQ_CODE = 123
    }
}

private fun removePermissionFragment(context: Context) {
    val activity = context.scanForActivity() as? AppCompatActivity ?: return
    val fragment = activity.supportFragmentManager.findFragmentByTag("permission_helper") ?: return
    activity.supportFragmentManager
        ?.beginTransaction()
        ?.remove(fragment)
        ?.commitAllowingStateLoss()
}

class PermissionResultReceiver(private val callback: PermissionCallback) :
    ResultReceiver(Handler()) {
    override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
        super.onReceiveResult(resultCode, resultData)
        if (resultCode == PERMISSION_RES_CODE && resultData != null) {
            when (resultData.getString(KEY)) {
                GRANTED -> callback.onGranted()
                DENIED -> callback.onDenied()
            }
        }
    }

    companion object {
        const val KEY = "key"
        const val GRANTED = "granted"
        const val DENIED = "denied"
    }
}

interface PermissionCallback {
    fun onGranted()
    fun onDenied()
}

private const val PERMISSION_NAMES = "permission_names"
private const val PERMISSION_ASK_AGAIN = "permission_ask_again"
private const val PERMISSION_RESULT_RECEIVER = "permission_result_receiver"
private const val PERMISSION_RES_CODE = 121


fun Context?.scanForActivity(): Activity? = when (this) {
    is Activity -> this
    is ContextWrapper -> baseContext.scanForActivity()
    else -> null
}

fun AppCompatActivity.addFragment(fragmentPair: Pair<Fragment, String>) {
    supportFragmentManager.beginTransaction().apply {
        add(fragmentPair.first, fragmentPair.second)
        commit()
    }
    supportFragmentManager.executePendingTransactions()
}

const val PREF_FILE_PERMISSIONS = "permissions"

fun String.setAskedForPermissionEarlier(context: Context, didAskEarlier: Boolean) {
    context.getSharedPreferences(PREF_FILE_PERMISSIONS, 0)
        .edit().putBoolean(this, didAskEarlier).apply()
}

fun String.didAskedForPermissionEarlier(context: Context): Boolean {
    return context.getSharedPreferences(PREF_FILE_PERMISSIONS, 0)
        .getBoolean(this, false)
}
