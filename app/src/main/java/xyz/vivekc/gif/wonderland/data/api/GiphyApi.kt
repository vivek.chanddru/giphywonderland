package xyz.vivekc.gif.wonderland.data.api

import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query
import xyz.vivekc.gif.wonderland.data.model.GifResponse

interface GiphyApi {

    @GET("trending")
    fun getTrendingGifs(@Query("limit") limit: Int, @Query("offset") startPos: Int): Flowable<GifResponse>

    @GET("search")
    fun getSearchResult(@Query("q") searchStr: String): Flowable<GifResponse>
}